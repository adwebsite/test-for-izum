$(function() {

$(".form_reg").hide();

$(".mode").change(function(){
	var mode = $(this).val();
	if (mode=='reg') {
		$(".form_reg").show();
		$(".form_auth").hide();
	} else {
		$(".form_reg").hide();
		$(".form_auth").show();
	}
});

$(".exit").click(function(){
	$.ajax({
		url: "/check.php",		
		dataType: 'json',
		data : "mode=exit",
		type : "POST",
		async: false,
		success: function(data){			
			if (data.res==0) {
				$("#login_error").html("");
				window.location="/";
			} else {
				$("#login_error").html("<font color='red'>"+data.error+"</font>");
			}				
		}	 
	});	
});	

$(".lng").click(function(){
	$.ajax({
		url: "/check.php",		
		dataType: 'json',
		data : "mode=lng&lng="+this.rel,
		type : "POST",		
		success: function(data){			
			document.location.reload();				
		}	 
	});	
});	


$("#authform").submit(function(){
	login_username = $("#login_username").val();
	login_password = $("#login_password").val();
	val = false;	
	$.ajax({
		url: "/check.php?mode=auth&username="+login_username+"&password="+login_password,
		dataType: 'json',		
		type : "POST",
		async: false,		
		success: function(data){			
			if (data.res==0) {
				$("#login_error").html("");
				val = false;
				window.location="/";
			} else {
				$("#login_error").html("<font color='red'>"+data.error+"</font>");						
			}				
		}	 
	});		
	return val;
})


$("#regform").submit(function(){
	login_reg = $("#username_reg").val();
	login_email = $("#email_reg").val();
	login_pass = $("#password_reg").val();
	login_pass2 = $("#password_reg2").val();	
	var fd = new FormData();			
	fd.append("pic", $("#ft")[0].files[0]);	
	fd.append("mode","reg");
	fd.append("username",login_reg);
	fd.append("email",login_email);
	fd.append("password",login_pass);
	fd.append("password2",login_pass2);
	val = false;
	$.ajax({
		url: "check.php",		
		dataType: 'json',		
		data : fd,		
		type : "POST",
		async: false,
		processData: false,
		contentType: false,
		success: function(data){						
					if (data.res==0) {
						$("#login_error_reg").html("");
						window.location="/";
						val = false;						
					} else {
						$("#login_error_reg").html("<font color='red'>"+data.error+"</font>");
					}												
		}								 
	});		
	return val;
})

})