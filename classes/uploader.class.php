<?php
class uploader
{
	public $res;	
	public $files; // Переменная $_FILES	
	private $path; // Путь до места где будет храниться
	private $current_dir; // текущий путь	
	
	function __construct($files,$current_dir) {	
		$this->files		= $files;		
		$this->current_dir	= $current_dir;		
		$this->res = 0; // Все ок			
	}
	
	public function getExt($filename) { 
		$path_info = pathinfo($filename); 
		return strtolower($path_info['extension']); 
	}
		
	public function load() {		
		$allow_ext = array("jpg","gif","png");
		$ext = $this->getExt($this->files['name']);
		if ($this->files['name']!="" &&	(in_array($ext,$allow_ext))) {			
			$now = md5(uniqid(rand().mktime(),1));//
			$name = $this->current_dir."/".$now.".".$ext; // имена у всех - случайные			
			if (move_uploaded_file($this->files['tmp_name'],$name)) {									
				chmod($name,0644);												
				$this->res = 0; // Успешно
			} else {
				$this->res = 8; // Не удалось загрузить файл
			}
		} else {
			$this->res = 9; // Нет  имени файла или расширение не из списка
		}
		$outs = array("res"=>($this->res),"url_attach"=>$name);
	return $outs;
	}			
}
?>