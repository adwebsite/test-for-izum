<?php
class users {
	public $res;
	public $mydb;
	public $id_user;
	protected $info;
	private $user;
	private $fields;
	private $check;	
	// При инициализации класс проверяет наличие в $info обязательных полей $fields (черз запятую)

	function __construct($mydb,$info,$fields="",$check=true,$check2=false) {	
		$this->mydb = $mydb;
		$this->info = $info;
		$this->fields = $fields;
		$this->check = $check;
		$this->id_user = 0;
		$mult = 1;		
		$this->res = 0; // Все ок			
		if ($this->check_field($fields,$check2)) {
			if ($check) {
				$this->user =  $this->mydb->query("SELECT *, COUNT(id) AS c FROM %perfix%st_users WHERE 
				username=".sql_quote($this->info['username'])." && password='".md5($this->info['password'])."'");
				if ($this->user['c']==0) {
					$this->res = 7; // Неверная пара логин/пароль
				}			
			}
		} else {
			$this->res = 1; // не все обязат параметры переданы
		}		
	}
	
	// Проверка обязательных полей
	public function check_field($f,$c) {
		$mult = 1; $this->res = 0;
		if (!empty($f)) {
			$param = explode(",",trim($f));
			foreach ($param as $p) {				 
				$mult = $mult * (isset($this->info[trim($p)]));
			}			
			// Все поля есть и флаг стоит сделать проверки полей, делаем проверки
			if ($mult==1 && $c) {
				foreach ($param as $p) {
					$p = trim($p);
					switch  ($p) {
					case 'username':
						if (mb_strlen($this->info['username'])<3) {
							$this->res = 2;
							break 2;
						} else {
							$t = $this->user =  $this->mydb->query("SELECT COUNT(id) AS s FROM %perfix%st_users WHERE 
							username=".sql_quote($this->info['username']));
							if ($t['s']>0) {
								$this->res = 3;
								break 2;
							}
						}
					break 1;
					case 'email':						
						$regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]{2,})+$/i';						
						if (!preg_match($regex, $this->info['email'])) {						
							$this->res = 4;
							break 2;
						}
					break 1;
					case 'password':
						if (mb_strlen($this->info['password'])<6) {
							$this->res = 5;
							break 2;
						} else {
							if ($this->info['password']!=$this->info['password2']) {
								$this->res = 6;
								break 2;
							}
						}
					break 1;
					}
				}
			}
		}
		return $mult;
	}

// считаем что все проверки прошли, регистрируем пользователя с переданными данными	
	public function register_user() {		
		// Записываем в БД
		$id_user = $this->mydb->query_ins("INSERT INTO %perfix%st_users 
		(username, password, email) 
		VALUES(
		".sql_quote($this->info['username']).",		
		'".md5($this->info['password'])."',
		".sql_quote($this->info['email']).")");		
		// Авторизуем через сессии
		$res = array();
		$res['id_user'] = $id_user;
		$res['username'] = $this->info['username'];		
		$res['email'] = $this->info['email'];	
		$res['id'] = $id_user;
		$this->id_user = $id_user;
		$_SESSION['userinfo'] = $res;
	}

	public function attach_file($file) {		
		// Обновляем в БД поле img
		if ($this->id_user!=0) {
			$this->mydb->query("UPDATE %perfix%st_users 
			SET img = '.$file.' WHERE id = ".intval($this->id_user));		
			$_SESSION['userinfo']['img'] = $file;
		}
	}
	
	public function autorization_user() {
		if ($c =  $this->mydb->query("SELECT * FROM %perfix%st_users WHERE 
		username=".sql_quote($this->info['username'])." 
		&& password='".md5($this->info['password'])."'")) {	
			$this->res = 0; // Удачно авторизовались			
			// Авторизуем через сессии
			$_SESSION['userinfo'] = $c;		
		} else	{
			$this->res = 7; // Неверная пара лог/пасс
		}
	}	
}

?>