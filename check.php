<?php		
session_start();
// Язык формы
$lng = (isset($_SESSION['lng'])) ? $_SESSION['lng'] : "ru";

@error_reporting(E_ERROR | E_PARSE);
if (isset($_POST['mode'])) {
	$mode = ($_POST['mode']);	
	include_once('config.php');		
	// авто-подключение необходимых классов
	spl_autoload_register(function ($class) {
		include_once('classes/' . $class . '.class.php');		
	});		
	// Подключаемся к БД
	$mydb = new MySQL();
	$mydb->connect($host,$base,$user,$pass,$char,$prfx);
	switch ($mode) {
		case "lng":
			$_SESSION['lng'] = $_POST['lng'];
		break 1;
		case "reg": // Проверка при регистрации
			$users = new users($mydb,$_POST,"username,email,password,password2",false,true); 			
			// не проверяем авторизацию - но проверяем поля			
			if ($users->res==0) {
				$users->register_user();				
				if (isset($_FILES['pic'])) {
					$uploader = new uploader($_FILES['pic'],"images");
					$a = $uploader->load();
					if ($a['res']==0) {
						// Если картинка загрузилась успешно - привязываем её к пользователю
						$users->attach_file($a['url_attach']);
					}
				}				 
			}	
			$out = $users->res;
		break 1;

		case "auth": // Авторизация
			$users = new users($mydb,$_POST,"username,password",true,false); 			
			// проверяем авторизацию - не проверяем поля			
			if ($users->res==0) {
				$users->autorization_user();
			}			
			$out = $users->res;
		break 1;	

		case "exit": // Выход
			if (isset($_SESSION['userinfo'])) {
				unset($_SESSION['userinfo']);
				$out = 0;
			} else {
				$out = 8;				
			}
		break 1;			
	}		
	
	$error_msg = $error[$out][$lng];
	
	echo json_encode(array("res"=>$out,"error"=>$error_msg));
}
?>