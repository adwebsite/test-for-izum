<?php
	$host = "localhost";    	// Хост
	$base = "base";		// База БД
	$user = "user";		// Пользователь
	$pass = 'pass'; 	// Пароль пользователя к БД
	$char = "utf8";     // Кодировка БД
	$prfx = "";      	// Перфикс таблиц 
	
	
	
	$error[1]['ru'] = "Не все обязательные поля заполнены";
	$error[1]['en'] = "Requered field is not passed";
	
	$error[2]['ru'] = "Логин не менее 3-х символов";
	$error[2]['en'] = "Length field login <3 symbols";
	
	$error[3]['ru'] = "Этот логин уже занят! Выберите другой";
	$error[3]['en'] = "It is login busy";
	
	$error[4]['ru'] = "Неверный email";
	$error[4]['en'] = "Incorrect email";
	
	$error[5]['ru'] = "Пароль не менее 6 символов!";
	$error[5]['en'] = "Length password <6 symbols";	
	
	$error[6]['ru'] = "Пароль и его подтверждение не совпадают!";
	$error[6]['en'] = "Password and confirmation is not correct";	
	
	$error[7]['ru'] = "Неверная пара логин/пароль";
	$error[7]['en'] = "Incorrect login/password";
	
	$error[8]['ru'] = "Выход не осуществлен";
	$error[8]['en'] = "It's not exit";
?>