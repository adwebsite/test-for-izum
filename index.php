<?php	
	session_start();	
	// Язык формы
	$lng = (isset($_SESSION['lng'])) ? $_SESSION['lng'] : "ru";	
	// тестовое задание для IZUM					
	// Начало работы скрипта. Смотрим на входные параметры
	if (isset($_SESSION['userinfo']) && intval($_SESSION['userinfo']['id'])>0) {
		// Есть сесссия - показываем профиль		
		$variant = "";
		if (isset($_SESSION['userinfo']['img']) && $_SESSION['userinfo']['img']!="") {
			$variant = '<img src="'.$_SESSION['userinfo']['img'].'">';
		}
		$title['ru'] = 'Профиль пользователя';
		$title['en'] = 'Profile user';	
		$content['ru'] = '<div class="container">
					<h1>Профиль пользователя</h1>
					<hr/>
					<p><b>RU</b>::<a href="#" rel="en" class="lng">ENG</a></p>
					<p>Информация по пользователю</p>
					<hr/>
					<ul>
						<li>Пользователь: <b>'.$_SESSION['userinfo']['username'].'</b></li>
						<li>E-mail: <b>'.$_SESSION['userinfo']['email'].'</b></li>						
					</ul>
					'.$variant.'
					<hr/>
					<a href="#" class="exit">Выйти</a>';		
		$content['en'] = '<div class="container">
					<h1>Profile user</h1>
					<hr/>
					<p><b>RU</b>::<a href="#" rel="en" class="lng">ENG</a></p>
					<p>Information of user</p>
					<hr/>
					<ul>
						<li>Username: <b>'.$_SESSION['userinfo']['username'].'</b></li>
						<li>E-mail: <b>'.$_SESSION['userinfo']['email'].'</b></li>						
					</ul>
					'.$variant.'
					<hr/>
					<a href="#" class="exit">Exit</a>';							
	} else {
		$title['ru'] = 'Авторизация/регистрация';
		$title['en'] = 'Authorization/registration';
		
		$content['ru'] = '<div class="container">
					<h1>Форма регистрации/авторизации</h1>
					<hr/>
					<p><b>RU</b>::<a href="#" rel="en" class="lng">ENG</a></p>
					<p>Выберите необходимый режим</p>					
					<input type="radio" name="mode" value="auth" id="auth" class="mode" checked>
					<label for="auth">Авторизация</label>&nbsp;&nbsp;
					<input type="radio" name="mode" value="reg" id="reg" class="mode">
					<label for="reg">Регистрация</label><br/>
					<hr/>
					<div class="form_auth">
						<form action="" method="post" id="authform">
							<b>Вход на сайт</b>								
							<div id="login_error"></div>
							<input type="text" id="login_username" name="username" placeholder="Логин"><br/>
							<input type="password" id="login_password" name="password" placeholder="Пароль"><br/>
							<input type="submit" value="Войти"> 
						</form>
					</div>
					<div class="form_reg">
						<form action="" name="regform" method="post" id="regform" enctype="multipart/form-data">
							<b>Регистрация</b>
							<div id="login_error_reg"></div>
							<input type="text" id="username_reg" name="username" placeholder="Логин"><br/>
							<input type="text" id="email_reg" name="email" placeholder="e-mail"><br/>
							<input type="password" id="password_reg" name="password" placeholder="Пароль" class="pass"><br/>
							<input type="password" id="password_reg2" name="password2" placeholder="Повтор пароля" class="pass"><br/>
							<label for="ft">Файл (gif, png, jpg)</label>&nbsp;<input type="file" name="ft" id="ft">
							<input type="submit" value="Зарегистрировать">
						</form>
					</div>	
				</div>';
		$content['en'] = '<div class="container">
					<h1>Form registration/authorization</h1>
					<hr/>
					<p><a href="#" rel="ru" class="lng">RU</a>::<b>ENG</b></p>
					<p>Choose the necessary mode</p>
					<input type="radio" name="mode" value="auth" id="auth" class="mode" checked>
					<label for="auth">Authorization</label>&nbsp;&nbsp;
					<input type="radio" name="mode" value="reg" id="reg" class="mode">
					<label for="reg">Registration</label><br/>
					<hr/>
					<div class="form_auth">
						<form action="" method="post" id="authform">
							<b>Entrance on the website</b>								
							<div id="login_error"></div>
							<input type="text" id="login_username" name="username" placeholder="Username"><br/>
							<input type="password" id="login_password" name="password" placeholder="Password"><br/>
							<input type="submit" value="Login"> 
						</form>
					</div>
					<div class="form_reg">
						<form action="" name="regform" method="post" id="regform" enctype="multipart/form-data">
							<b>Registration</b>
							<div id="login_error_reg"></div>
							<input type="text" id="username_reg" name="username" placeholder="Username"><br/>
							<input type="text" id="email_reg" name="email" placeholder="e-mail"><br/>
							<input type="password" id="password_reg" name="password" placeholder="Password" class="pass"><br/>
							<input type="password" id="password_reg2" name="password2" placeholder="Repeat password" class="pass"><br/>
							<label for="ft">Image file (gif, png, jpg)</label>&nbsp;<input type="file" name="ft" id="ft">
							<input type="submit" value="Registration">
						</form>
					</div>	
				</div>';
		// Вывод формы для авторизации/регистрации
		
	}	
	echo '
		<!DOCTYPE html>
		<html xmlns="http://www.w3.org/1999/xhtml" lang="ru">
		<head>
			<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
			<title>'.$title[$lng].'</title>
			<meta name="description" content="'.$title[$lng].'" />			
			<meta name="Resource-type" content="document" />
			<meta name="document-state" content="dynamic" />
			<meta name="Robots" content="index,follow" />
			<meta name="viewport" content="width=device-width, initial-scale=1">			
			<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />				
			<script type="text/javascript" src="/js/jquery-1.10.2.js"></script>				
			<script type="text/javascript" src="/js/init.js"></script>
		</head>
		<body>
			<header class="header">
				'.$content[$lng].'
			</header>
		</body>
		</html>'
?>